﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.Bot.Connector;
using Newtonsoft.Json;
using Microsoft.Bot.Builder.Dialogs;

namespace JadeDigital.Conversational.SuperBot
{
    [BotAuthentication]
    public class MessagesController : ApiController
    {
        /// <summary>
        /// POST: api/Messages
        /// Receive a message from a user and reply to it
        /// </summary>
        public async Task<HttpResponseMessage> Post([FromBody]Activity activity)
        {
            try
            {
                if (activity.Type == ActivityTypes.Message)
                {
                    if (activity.Text.ToLower().Contains("resetuserstate"))
                    {
                        ResetUserState(activity);
                    }
                    else
                    {
                        await activity.SendTypingActivity();
                        await IdentifyUser(activity);
                        await Conversation.SendAsync(activity, MakeRootDialog);
                    }
                }
                else
                {
                    HandleSystemMessage(activity);
                }
                var response = Request.CreateResponse(HttpStatusCode.OK);
                return response;
            }
            catch (Exception e)
            {
                //var connector = new ConnectorClient(new Uri(activity.ServiceUrl));
                //await connector.Conversations.ReplyToActivityAsync(activity.CreateReply(e.ToString()));

                var response = Request.CreateResponse(HttpStatusCode.InternalServerError);
                response.Headers.Add("Exception", e.Message);
                return response;
            }
        }

        private void ResetUserState(Activity activity)
        {
            StateClient stateClient = activity.GetStateClient();
            BotData userData = stateClient.BotState.GetUserData(activity.ChannelId, activity.From.Id);
            userData.SetProperty<string>("SecurityDetails", null);
            stateClient.BotState.DeleteStateForUser(activity.ChannelId, activity.From.Id);
            //stateClient.BotState.DeleteStateForUserAsync(activity.ChannelId, activity.From.Id);
        }

        internal static IDialog<object> MakeRootDialog()
        {
            return Chain.From(() => new Lobby());
        }

        private Activity HandleSystemMessage(Activity message)
        {
            if (message.Type == ActivityTypes.DeleteUserData)
            {
                // Implement user deletion here
                // If we handle user deletion, return a real message
            }
            else if (message.Type == ActivityTypes.ConversationUpdate)
            {
                // Handle conversation state changes, like members being added and removed
                // Use Activity.MembersAdded and Activity.MembersRemoved and Activity.Action for info
                // Not available in all channels
            }
            else if (message.Type == ActivityTypes.ContactRelationUpdate)
            {
                // Handle add/remove from contact lists
                // Activity.From + Activity.Action represent what happened
            }
            else if (message.Type == ActivityTypes.Typing)
            {
                // Handle knowing tha the user is typing
            }
            else if (message.Type == ActivityTypes.Ping)
            {
            }

            return null;
        }

        private static async Task IdentifyUser(Activity activity)
        {
            if (activity.From == null || activity.From.Name == null || activity.From.Id == null)
            {
                activity.From = new ChannelAccount { Id = "DirectLine (Test)", Name = "Anonymous Webuser" };
            }

            StateClient stateClient = activity.GetStateClient();
            BotData userData = await stateClient.BotState.GetUserDataAsync(activity.ChannelId, activity.From.Id);

            string fromName = activity.From.Name == null ? "Test User" : activity.From.Name;
            string[] name = fromName.Split();
            userData.SetProperty("UserName", fromName);
            userData.SetProperty("UserId", activity.From.Id);
            userData.SetProperty("Firstname", name[0]);
            userData.SetProperty("Lastname", name.Length > 1 ? name[1] : "");
            userData.SetProperty("Channel", activity.ChannelId);
            userData.SetProperty("ServiceURL", activity.ServiceUrl);

            await stateClient.BotState.SetUserDataAsync(activity.ChannelId, activity.From.Id, userData);
        }

    }
}