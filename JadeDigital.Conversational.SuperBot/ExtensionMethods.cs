﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace JadeDigital.Conversational.SuperBot
{
    public static class ExtensionMethods
    {
        /**
         *Method to send the typing elipses to a conversation from the messages controller 
         */
        public static async Task SendTypingActivity(this Activity activity)
        {
            ConnectorClient connector = new ConnectorClient(new Uri(activity.ServiceUrl));
            Activity typingActivity = activity.CreateReply();
            typingActivity.Type = ActivityTypes.Typing;
            await connector.Conversations.ReplyToActivityAsync(typingActivity);
        }

        /**
         *Method to send the typing elipses to a conversation from dialogs
         */
        public static async Task SendTypingActivity(this IDialogContext context)
        {
            IMessageActivity typingMessage = context.MakeMessage();
            typingMessage.Type = ActivityTypes.Typing;
            await context.PostAsync(typingMessage);
        }
    }
}