﻿using Autofac;
using JadeDigital.Conversational.SuperBot.Controllers;
using Microsoft.Bot.Builder.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;

namespace JadeDigital.Conversational.SuperBot
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {

            //var builder = new ContainerBuilder();
            //builder.RegisterModule(new DefaultExceptionMessageOverrideModule());
            //builder.Update(Conversation.Container);
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}
