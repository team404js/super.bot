﻿using Microsoft.Bot.Builder.FormFlow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JadeDigital.Conversational.SuperBot.Scenarios
{
    [Serializable]
    public class EmailDetails
    {
        [Prompt("What is your new email address?")]
        [Pattern(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])+")]
        [Template(TemplateUsage.NotUnderstood, "Hmm, that doesn't look like a valid email address. Can you try this again please?",
            "I'm sorry, that doesn't look like a valid {&}. Try again or type 'quit' to cancel this form.")]
        public string Email;

        public static IForm<EmailDetails> BuildForm()
        {
            return new FormBuilder<EmailDetails>()
                .AddRemainingFields()
                .Confirm("No verification necessary", state => false)
                .Build();
        }
    }
}