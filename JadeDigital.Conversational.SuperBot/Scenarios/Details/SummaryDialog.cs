﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Luis.Models;
using System.Threading;

namespace JadeDigital.Conversational.SuperBot.Scenarios
{
    [Serializable]
    public class SummaryDialog : Lobby, IConversationDialog
    {
        public async Task StartDialog(IDialogContext context, LuisResult result)
        {
            await new ConfirmIdentityDialog(onConfirmIdentityCompleted).StartDialog(context, result);
        }

        private async Task onConfirmIdentityCompleted(IDialogContext context, bool success)
        {
            await context.PostAsync(Strings.Summary_Acknowledgement);
            await context.PostAsync(string.Format(Strings.Summary_PersonalDetails, context.UserData.Get<string>("UserName")));

            var currentDate = DateTime.Today;
            var lastDayOfMonth = DateTime.DaysInMonth(currentDate.Year, currentDate.Month);
            var nextContribution = new DateTime(currentDate.Year, currentDate.Month, lastDayOfMonth);
            await context.PostAsync(string.Format(Strings.Summary_Position, nextContribution));

            Thread.Sleep(2000);
            await new CrossSellDialog().StartDialog(context, null);
        }
    }
}