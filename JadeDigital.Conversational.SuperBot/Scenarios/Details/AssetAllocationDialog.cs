﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Luis.Models;

namespace JadeDigital.Conversational.SuperBot.Scenarios
{
    [Serializable]
    public class AssetAllocationDialog : Lobby, IConversationDialog
    {
        public async Task StartDialog(IDialogContext context, LuisResult result)
        {
            await context.PostAsync(Strings.AssetAllocation);
            context.Wait(MessageReceived);
        }
    }
}