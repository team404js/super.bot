﻿using Microsoft.Bot.Builder.FormFlow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JadeDigital.Conversational.SuperBot.Scenarios
{
    [Serializable]
    public class PhoneDetails
    {
        [Prompt("What is your current phone number?")]
        public string Phone;

        public static IForm<PhoneDetails> BuildForm()
        {
            return new FormBuilder<PhoneDetails>()
                .AddRemainingFields()
                .Confirm("No verification necessary", state => false)
                .Build();
        }
    }
}