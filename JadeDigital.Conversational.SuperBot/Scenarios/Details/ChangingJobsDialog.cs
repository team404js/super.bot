﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Luis.Models;
using Microsoft.Bot.Builder.FormFlow;

namespace JadeDigital.Conversational.SuperBot.Scenarios
{
    [Serializable]
    public class ChangingJobsDialog : Lobby, IConversationDialog
    {
        public async Task StartDialog(IDialogContext context, LuisResult result)
        {
            await new ConfirmIdentityDialog(onConfirmIdentityCompleted).StartDialog(context, result);
        }

        private async Task onConfirmIdentityCompleted(IDialogContext context, bool success)
        {
            if (!success)
            {
                context.Wait(MessageReceived);
                return;
            }

            await context.PostAsync(Strings.ChangingJobs_Acknowledgment);
            PromptDialog.Confirm(context, OnEmailConfirmCompleted,
                new PromptOptions<string>(Strings.ChangingJobs_EmailCorrect, options: new List<string> { "Yes", "No" }));
        }

        private async Task OnEmailConfirmCompleted(IDialogContext context, IAwaitable<bool> result)
        {
            bool response = await result;

            if(response)
            {
                await context.PostAsync(Strings.ChangingJobs_FinishedWithoutUpdating);
                context.Wait(MessageReceived);
                return;
            }

            var emailForm = new FormDialog<EmailDetails>(new EmailDetails(), EmailDetails.BuildForm, FormOptions.PromptInStart);
            context.Call(emailForm, NewEmailComplete);
        }

        private async Task NewEmailComplete(IDialogContext context, IAwaitable<EmailDetails> result)
        {
            EmailDetails emailDetails;
            try
            {
                emailDetails = await result;

                if (emailDetails == null)
                {
                    await context.PostAsync("New Email Form returned empty response!");
                    context.Wait(MessageReceived);
                    return;
                }

                await context.PostAsync(Strings.ChangingJobs_Finished);
            }
            catch (OperationCanceledException ex)
            {
                await context.PostAsync(ex.Message);
            }

            context.Wait(MessageReceived);
        }
    }
}