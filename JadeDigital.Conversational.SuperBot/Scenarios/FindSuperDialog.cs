﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Luis.Models;
using Microsoft.Bot.Builder.FormFlow;

namespace JadeDigital.Conversational.SuperBot.Scenarios
{
    [Serializable]
    public class FindSuperDialog : Lobby, IConversationDialog
    {
        public async Task StartDialog(IDialogContext context, LuisResult result)
        {
            await new ConfirmIdentityDialog(onConfirmIdentityCompleted).StartDialog(context, result);
        }

        private async Task onConfirmIdentityCompleted(IDialogContext context, bool success)
        {
            if(!success)
            {
                context.Wait(MessageReceived);
                return;
            }

            await context.PostAsync(Strings.FindSuper_Acknowledgement);
            PromptDialog.Confirm(context, OnConsentAcknowledgement, Strings.FindSuper_Consent);
        }

        private async Task OnConsentAcknowledgement(IDialogContext context, IAwaitable<bool> result)
        {
            bool response = await result;

            if (response)
                await context.PostAsync(Strings.FindSuper_Success);
            else
                await context.PostAsync(Strings.FindSuper_Failed);


            await context.PostAsync(Strings.Phone_Start);
            var emailForm = new FormDialog<PhoneDetails>(new PhoneDetails(), PhoneDetails.BuildForm, FormOptions.PromptInStart);
            context.Call(emailForm, PhoneUpdated);
        }

        private async Task PhoneUpdated(IDialogContext context, IAwaitable<PhoneDetails> result)
        {
            PhoneDetails phoneDetails;
            try
            {
                phoneDetails = await result;

                if (phoneDetails == null)
                {
                    await context.PostAsync("Update Phone Form returned empty response!");
                    context.Wait(MessageReceived);
                    return;
                }

                await context.PostAsync(string.Format(Strings.Phone_Updated, phoneDetails.Phone));
            }
            catch (OperationCanceledException ex)
            {
                await context.PostAsync(ex.Message);
            }

            context.Wait(MessageReceived);
        }

    }
}