﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Luis.Models;

namespace JadeDigital.Conversational.SuperBot.Scenarios
{
    [Serializable]
    public class FinancialAdviseDialog : Lobby, IConversationDialog
    {
        public async Task StartDialog(IDialogContext context, LuisResult result)
        {
            await context.PostAsync(Strings.FinancialAdvise_Acknowledgement);

            PromptDialog.Choice(context, OnOptionSelected,
                new PromptOptions<string>(Strings.FinancialAdvise_Options, 
                options: new List<string> { "Retirement", "Transition to retirement", "Changing circumstances" }));
        }

        private async Task OnOptionSelected(IDialogContext context, IAwaitable<string> result)
        {
            string response = await result;

            await context.PostAsync(string.Format(Strings.FinancialAdvise_PlanCall, response.ToLower()));
            PromptDialog.Choice(context, OnTimeSelected,
                new PromptOptions<string>(Strings.FinancialAdvise_PreferredTime,
                options: new List<string> { "Morning (8:00-12:00)", "Afternoon (13:00-17:00)", "Evenings (17:00-20:00)" }));
        }

        private async Task OnTimeSelected(IDialogContext context, IAwaitable<string> result)
        {
            string response = await result;
            await context.PostAsync(string.Format(Strings.FinancialAdvise_Finished, response.ToLower()));

            context.Wait(MessageReceived);
        }
    }
}