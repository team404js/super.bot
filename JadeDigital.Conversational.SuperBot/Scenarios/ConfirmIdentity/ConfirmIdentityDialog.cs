﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using Microsoft.Bot.Builder.Luis.Models;
using System;
using System.Threading.Tasks;

namespace JadeDigital.Conversational.SuperBot.Scenarios
{
    public delegate Task OnConfirmIdentityCompleted(IDialogContext context, bool success);

    [Serializable]
    public class ConfirmIdentityDialog : Lobby, IConversationDialog
    {
        private OnConfirmIdentityCompleted _callback;

        public ConfirmIdentityDialog(OnConfirmIdentityCompleted callback)
        {
            _callback = callback;
        }

        public async Task StartDialog(IDialogContext context, LuisResult result)
        {
            SecurityDetails securityDetails;

            if (context.UserData.TryGetValue("SecurityDetails", out securityDetails))
            {
                await _callback(context, true);
                return;
            }

            await context.PostAsync(Strings.ConfirmIdentity_Info);

            var securityConfirmation = new FormDialog<SecurityDetails>(new SecurityDetails(), SecurityDetails.BuildForm, FormOptions.PromptInStart, result.Entities);
            context.Call(securityConfirmation, IdentityConfirmedComplete);
        }

        private async Task IdentityConfirmedComplete(IDialogContext context, IAwaitable<SecurityDetails> result)
        {
            SecurityDetails securityDetails;
            try
            {
                securityDetails = await result;

                if (securityDetails == null)
                {
                    await context.PostAsync("ConfirmIdentity returned empty response!");
                    await _callback(context, false);
                    return;
                }

                context.UserData.SetValue("SecurityDetails", securityDetails);
                await context.PostAsync(Strings.ConfirmIdentity_Confirmed);
                await _callback(context, true);
            }
            catch (OperationCanceledException)
            {
                await context.PostAsync(Strings.ConfirmIdentity_Failed);
                await _callback(context, false);
            }
        }
    }
}