﻿using Microsoft.Bot.Builder.FormFlow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JadeDigital.Conversational.SuperBot.Scenarios
{
    [Serializable]
    public class SecurityDetails
    {
        [Prompt("Please confirm your member number for me (hint: you find that on your statements and on any letters you receive from us)")]
        public string MemberNumber;

        [Prompt("Please confirm your date of birth")]
        public DateTime DateOfBirth;

        public static IForm<SecurityDetails> BuildForm()
        {
            return new FormBuilder<SecurityDetails>()
                .AddRemainingFields()
                .Confirm("No verification necessary", state => false)
                .Build();
        }
    }
}