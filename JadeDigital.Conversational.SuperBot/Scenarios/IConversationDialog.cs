﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Luis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JadeDigital.Conversational.SuperBot.Scenarios
{
    interface IConversationDialog
    {
        Task StartDialog(IDialogContext context, LuisResult result);
    }
}
