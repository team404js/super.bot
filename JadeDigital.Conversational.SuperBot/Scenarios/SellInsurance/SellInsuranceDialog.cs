﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using Microsoft.Bot.Builder.Luis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace JadeDigital.Conversational.SuperBot.Scenarios
{
    [Serializable]
    public class SellInsuranceDialog : Lobby, IConversationDialog
    {
        private InsuranceDetails insuranceDetails;

        public async Task StartDialog(IDialogContext context, LuisResult result)
        {
            var insuranceEntity = result.Entities.Where(x => x.Type == "InsuranceType").FirstOrDefault<EntityRecommendation>();
            var amountEntity = result.Entities.Where(x => x.Type == "builtin.number" || x.Type == "builtin.money").FirstOrDefault<EntityRecommendation>();

            insuranceDetails = new InsuranceDetails { Type = insuranceEntity == null ? null : insuranceEntity.Entity };
            if (amountEntity != null)
                insuranceDetails.Cover = Convert.ToDouble(amountEntity.Resolution["value"]);
            await new ConfirmIdentityDialog(onConfirmIdentityCompleted).StartDialog(context, result);
        }

        private async Task onConfirmIdentityCompleted(IDialogContext context, bool success)
        {
            if (!success)
            {
                context.Wait(MessageReceived);
                return;
            }

            var cover = string.IsNullOrEmpty(insuranceDetails.Type) ? "insurance" : insuranceDetails.Type;
            await context.PostAsync($"Ok, let's get you started with the quote for {cover}");

            var emailForm = new FormDialog<InsuranceDetails>(insuranceDetails, InsuranceDetails.BuildForm, FormOptions.PromptInStart);
            context.Call(emailForm, InsuranceCompleted);
        }

        private async Task InsuranceCompleted(IDialogContext context, IAwaitable<InsuranceDetails> result)
        {
            var details = await result;

            await context.PostAsync(string.Format(Strings.SellInsurance_Quote, details.Cover, details.Premium));
            PromptDialog.Confirm(context, OnApplicationConfirmed, Strings.SellInsurance_Apply);
        }

        private async Task OnApplicationConfirmed(IDialogContext context, IAwaitable<bool> result)
        {
            var response = await result;

            await context.PostAsync(response ? Strings.SellInsurance_Accepted : Strings.SellInsurance_Declined);
            context.Wait(MessageReceived);
        }
    }
}