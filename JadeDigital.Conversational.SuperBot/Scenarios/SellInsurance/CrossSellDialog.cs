﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Luis.Models;
using Microsoft.Bot.Connector;

namespace JadeDigital.Conversational.SuperBot.Scenarios
{
    [Serializable]
    public class CrossSellDialog : Lobby, IConversationDialog
    {
        public async Task StartDialog(IDialogContext context, LuisResult result)
        {
            if(HasRunBefore(context))
            {
                context.Wait(MessageReceived);
                return;
            }

            await context.PostAsync(Strings.CrossSell_Start);
            await context.SendTypingActivity();

            var activity = CreateCardsForCrossSell(context);
            await context.PostAsync(activity);

            context.Wait(MessageReceived);
        }

        private bool HasRunBefore(IDialogContext context)
        {
            bool crossSellAttempted = false;
            context.UserData.TryGetValue("CrossSellAttempted", out crossSellAttempted);

            if (crossSellAttempted) return true;

            context.UserData.SetValue("CrossSellAttempted", true);
            return false;
        }

        private IMessageActivity CreateCardsForCrossSell(IDialogContext context)
        {
            var reply = context.MakeMessage();
            reply.Attachments = new List<Attachment>();

            //cardImages.Add(new CardImage(url: "http://lifestyleassurance.net/wp-content/uploads/2012/09/Income-Protection-Insurance.jpg"));
            HeroCard cardDeath = new HeroCard()
            {
                Title = "Death & Terminal Illness",
                Text = "Indicative estimate: $67/month",
                Tap = new CardAction { Value = "http://www.Excelsuper.com.au/personal/for-members/super/insurance-choices", Type = "openUrl" },
                Images = new List<CardImage> { new CardImage(url: "http://www.insure.com/imagesvr_ce/8706/cemetery320.jpg") },
                Buttons = new List<CardAction> { new CardAction { Value = "death", Type = "imBack", Title = "Quote" } }
            };
            reply.Attachments.Add(cardDeath.ToAttachment());

            HeroCard cardDisablement = new HeroCard()
            {
                Title = "Total & permanent disablement",
                Text = "Indicative estimate: $35/month",
                Tap = new CardAction { Value = "http://www.Excelsuper.com.au/personal/for-members/super/insurance-choices", Type = "openUrl" },
                Images = new List<CardImage> { new CardImage(url: "http://www.qbg.co.nz/media/1012/permanent-disability-cover.jpg") },
                Buttons = new List<CardAction> { new CardAction { Value = "total permanent disablement", Type = "imBack", Title = "Quote" } }
            };
            reply.Attachments.Add(cardDisablement.ToAttachment());

            HeroCard cardIP = new HeroCard()
            {
                Title = "Income protection",
                Text = "Indicative estimate: $18/month",
                Tap = new CardAction { Value = "http://www.Excelsuper.com.au/personal/for-members/super/insurance-choices", Type = "openUrl" },
                Images = new List<CardImage> { new CardImage(url: "http://lifestyleassurance.net/wp-content/uploads/2012/09/Income-Protection-Insurance.jpg") },
                Buttons = new List<CardAction> { new CardAction { Value = "income protection", Type = "imBack", Title = "Quote" } }
            };
            reply.Attachments.Add(cardIP.ToAttachment());
            reply.AttachmentLayout = "carousel";

            return reply;
        }
    }
}