﻿using Microsoft.Bot.Builder.FormFlow;
using System;

namespace JadeDigital.Conversational.SuperBot.Scenarios
{
    [Serializable]
    public class InsuranceDetails
    {
        [Prompt("What type of cover are you interested in?")]
        public string Type;

        [Prompt("For how much would you like to be covered for?")]
        [Template(TemplateUsage.NotUnderstood, "Hmm, that doesn't look right. Please enter an amount.",
            "I'm sorry, that doesn't look like a valid {&}. Try again or type 'quit' and we'll forget about it.")]
        public double Cover;

        [Prompt("Would you like us to deduct your premium from your contributions? {||}")]
        public bool? DeductFromContribution;

        public double Premium {
            get
            {
                if (Cover == 0) return 0;
                return Cover / 3254;
            }
        }

        public static IForm<InsuranceDetails> BuildForm()
        {
            return new FormBuilder<InsuranceDetails>()
                .AddRemainingFields()
                .Confirm("No verification necessary", state => false)
                .Build();
        }
    }
}