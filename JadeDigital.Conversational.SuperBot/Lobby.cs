﻿using JadeDigital.Conversational.SuperBot.Scenarios;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace JadeDigital.Conversational.SuperBot
{
    [Serializable]
    [LuisModel("49b7e194-2439-4b8c-b9d0-65621707da8c", "a06b0794564048b5bb7b484b9e9e3bc4")]
    public class Lobby : LuisDialog<object>
    {
        [LuisIntent("None")]
        public async Task None(IDialogContext context, LuisResult result)
        {
            await context.PostAsync(Strings.None);
            context.Wait(MessageReceived);
        }

        [LuisIntent("Greeting")]
        public async Task Greeting(IDialogContext context, LuisResult result)
        {
            var greeting = string.Format("Hi {0}, how can I help you today?",
                context.UserData.Get<string>("Firstname"));

            await context.PostAsync(greeting);
            context.Wait(MessageReceived);
        }

        [LuisIntent("ThankYou")]
        public async Task ThankYou(IDialogContext context, LuisResult result)
        {
            await context.PostAsync(Strings.ThankYou);
            context.Wait(MessageReceived);
        }

        [LuisIntent("FindSuper")]
        public async Task FindSuper(IDialogContext context, LuisResult result)
        {
            await new FindSuperDialog().StartDialog(context, result);
        }

        [LuisIntent("ChangingJobs")]
        public async Task ChangingJobs(IDialogContext context, LuisResult result)
        {
            await new ChangingJobsDialog().StartDialog(context, result);
        }

        [LuisIntent("FinancialAdvise")]
        public async Task FinancialAdvise(IDialogContext context, LuisResult result)
        {
            await new FinancialAdviseDialog().StartDialog(context, result);
        }

        [LuisIntent("Summary")]
        public async Task Summary(IDialogContext context, LuisResult result)
        {
            await new SummaryDialog().StartDialog(context, result);
        }

        [LuisIntent("SellInsurance")]
        public async Task SellInsurance(IDialogContext context, LuisResult result)
        {
            await new SellInsuranceDialog().StartDialog(context, result);
        }

        [LuisIntent("AssetAllocation")]
        public async Task AssetAllocation(IDialogContext context, LuisResult result)
        {
            await new AssetAllocationDialog().StartDialog(context, result);
        }

    }
}